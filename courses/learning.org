* Learning how to learn

One of the most useful [[https://www.coursera.org/learn/learning-how-to-learn/home/welcome][courses]] out there.  It is really wort to take,
even more than once.


** Course questions

*** ??

I try to take responsibly for my education, but I have huge trouble
with it.  My mind have huge tendency to wonder, especially when I
read.  Just after few sentences I can find myself thinking about
something else, or even more common, thinking "deep thoughts" about
what I'm reading.  In a way I tend to argue (apt) with text.  I
believe that sometimes such approach could be found beneficial, but in
my case it slows down my progress almost to a stop.  Sometimes it
takes me few days to go trough one page of text, which i far from
being ideal.

I have similar problems with "creative work" as programmer, when
trying to find solution to some problem (which almost is definition of
programming) I can easily find myself wondering, on some strange
escapade of knowledge gaining which quickly goes far beyond what I
needed to learn in first place.

I do not spend enough time practicing.  I've always had believed that
most important part of mastering something is really understanding it.
And quickly I've started settling just on understanding the concept.
Now I'm trying fight this really hard.  I tend to rewrite misspelled
words rather than correcting just one letter; and it goes exactly the
same with designing and writing programs (where a lot of correctness
is needed, both in syntax and semantics).  And I try to practice a lot
more, and with this a huge help i gamefication on platforms like
HackeRank, or fallowing my progress level on MOOC's.

*** practice breathing for at least 90 seconds

*Did your thoughts became clearer?*

I do think my thought became clearer, but I have to do it for much
shorter period of time.  90 seconds would just make my thought wander
again.

For some time now I had this idea that most people start thinking
about something else no matter what, it is very natural behavior for
my.  So if distracting yourself is inevitable, then only thing you
could do is to manage your distraction.  Listening to music would be
great example, it keeps your "unconscious" mind occupied with
something, so he wont go places you wouldn't like him to go, like
checking your email or thinking what's going on on Facebook.

If it's true, than breathing would be such distraction for distracting
part of my mind.

**** Mindfulness

Breathing could also be practice of conscious mind.  Or in more
pragmatic terms, practice of focus.  On this front it helps a lot,
even if it's hard at the beginning.

I've started looking into Mindfulness after this [[https://www.youtube.com/watch?v=FAcTIrA2Qhk&t=34s][video]].  And now
there is [[https://www.coursera.org/learn/mindfulness/][MOOC on Coursera]].
