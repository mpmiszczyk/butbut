
* The Human Representation Of Thought

A [[https://vimeo.com/115154289][talk]] by Bret Victor, who says much more on his [[http://worrydream.com/][blog]] (?)

It is a difficult talk.  On one hand it's hard not to agree with the
guy, but on the other much of what he is talking about is already
happening, and even without some fancy computer medium.

Yes we type all day, but programming isn't just typing.  That's just
the last part of the process.  Before that we think, tralk, draw,
create even models of abstraction.  And even with typing, code have
it's structure, it's rythm.  And even when you type, there is huge
amout to try and error in very enactive process.

Or at least it can be so.  In each of those fields we could improve.
Sometimes by imporoving language, most often by changing habits
(becoming more aware of our process).

Designing new medium is very big endeavor,  As humanity only few times
we managed to pull someting like this off, and I would guess most of
those ware coincidential.  So improving on language design might just
not happen.

I've always (to be honest not always, I've been in the other camp of
visual programming, which should make my arguments little more on
point) been fan of written world.  The level of expressiveness is just
enormous, way beond any other medium.  Visual languages are good, even
extremly good, but only on some subset of task.  The can easily fall
short, being either not able to express something, being too inexact,
or becoming to complex.

From my experience it would be perfect to be able to use both of
them.  Something similar to Flex, which utilized both ActionScript and
declarative fxml with data-flow binding semantics.

** Modes of understanding

Development of easli accesibel writen word concised us to
Visual/Symbolic means of working.  And symbolic doesn't just mean
word, but also drawings, even those are just symbols.

But drowing in most parts is tactile, and I would argue it is
spatial.

** Dynamic Medium






